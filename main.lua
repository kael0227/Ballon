-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

-- Your code here

--local is for a Variable
local background = display.newImageRect("background.png", 360, 570)
background.x = display.contentCenterX
background.y = display.contentCenterY

local platform = display.newImageRect( "platform.png", 300, 50 )
platform.x = display.contentCenterX
platform.y = display.contentHeight-30

local ballon = display.newImageRect( "balloon.png", 112, 112 )
ballon.x = display.contentCenterX
ballon.y = display.contentCenterY
ballon.alpha = 0.8

local physics = require( "physics" )
physics.start()

physics.addBody( platform, "static" )
physics.addBody( ballon, "dynamic", { radius=50, bounce=0.1 } )

local function pushBallon()
    ballon:applyLinearImpulse( -0.00006, -0.75, ballon.x, ballon.y )
end

ballon:addEventListener( "tap", pushBallon )
